package com.wang.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;

import javax.sql.DataSource;

/**
 * @author WangZiYan
 * @create 2023/10/9 15:06
 */

public class JDBCConfig {
    @Value("${driver}")
    String driver;
    @Value("${url}")
    String url;
    @Value("${user}")
    String username;
    @Value("${password}")
    String password;

    public DataSource dataSource(){
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }
}
