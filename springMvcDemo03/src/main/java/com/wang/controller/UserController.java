package com.wang.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author WangZiYan
 * @create 2023/10/9 15:20
 */
public class UserController {


    @PostMapping(value = "/param",produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String param(String name){
        System.out.println(name);
        return name;
    }
}
