package com.wang.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author WangZiYan
 * @create 2023/10/9 15:13
 */
@Configuration
@PropertySource("com.wang.controller")
public class SpringMvcConfig {

}
