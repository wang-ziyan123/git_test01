package com.wang.entity;

import java.io.Serializable;

/**
 * @author WangZiYan
 * @create 2023/10/9 8:57
 */
public class Role implements Serializable {

    private static final long serialVersionUID = 3654273463645821043L;
    private Integer id;

    private String roleName;

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", roleName='" + roleName + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
