

# 搭建SpringSSM

## 1. 创建maven项目

## 2.在por.xml加入所需依赖

```java
<dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.11</version>
      <scope>test</scope>
    </dependency>

    <!--spring依赖-->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-context</artifactId>
      <version>5.2.10.RELEASE</version>
    </dependency>
    <!--mysql依赖-->
    <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
      <version>8.0.11</version>
    </dependency>
    <!--druid数据库依赖-->
    <dependency>
      <groupId>com.alibaba</groupId>
      <artifactId>druid</artifactId>
      <version>1.2.16</version>
    </dependency>
    <!--spring-jdbc依赖-->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-jdbc</artifactId>
      <version>5.2.10.RELEASE</version>
    </dependency>
    <!--mybatis依赖-->
    <dependency>
      <groupId>org.mybatis</groupId>
      <artifactId>mybatis</artifactId>
      <version>3.5.7</version>
    </dependency>
    <!--mybatis -spring依赖-->
    <dependency>
      <groupId>org.mybatis</groupId>
      <artifactId>mybatis-spring</artifactId>
      <version>1.3.3</version>
    </dependency>
    <!--log4j依赖-->
    <dependency>
      <groupId>log4j</groupId>
      <artifactId>log4j</artifactId>
      <version>1.2.17</version>
    </dependency>
    <!--spring测试依赖-->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-test</artifactId>
      <version>5.2.10.RELEASE</version>
    </dependency>
    <!--aspectj AOP依赖-->
    <dependency>
      <groupId>org.aspectj</groupId>
      <artifactId>aspectjweaver</artifactId>
      <version>1.9.4</version>
    </dependency>
    <!--servlet依赖-->
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>javax.servlet-api</artifactId>
      <version>4.0.1</version>
    </dependency>
    <!--springmvc-->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-webmvc</artifactId>
      <version>5.2.10.RELEASE</version>
    </dependency>
  </dependencies>
```

## 3.创建包

#### 1.Config层

##### 1、SpringConfig

先给SpringConfig添加注解：@Configuration//配置类

然后添加扫描主键的注解，扫描com.wang文件下所有的主键

```java
@Configuration//配置类
@ComponentScan(value = "com.wang")
public class SpringConfig {
}
```

JDBCConfig配置完成后再SpringConfig中加入注解

```java
@PropertySource("jdbc.properties")//jdbc
```

MyBatisConfig配置完成后在SpringConfig中加入注解@Import导入JDBC和Mybatis的配置

```java
@Import({JDBCConfig.class,MyBatisConfig.class})
```

过滤掉扫描controller层

```java
excludeFilters = @ComponentScan.Filter
                (type = FilterType.ANNOTATION,
                        classes = Controller.class))//扫描主键,过去controller层
```

整合起来的SpringConfig是

```java
@Configuration//配置类
@ComponentScan(value = "com.wang",
        excludeFilters = @ComponentScan.Filter
                (type = FilterType.ANNOTATION,
                        classes = Controller.class))//扫描主键,过去controller层
@PropertySource("jdbc.properties")//jdbc
@Import({JDBCConfig.class,MyBatisConfig.class})
@EnableAspectJAutoProxy//开启aop注解
public class SpringConfig {
}
```



##### 2、JDBCConfig

在resources包中添加JDBC的配置文件

```java
public class JDBCConfig {
    @Value("${driver}")
    String driver;
    @Value("${url}")
    String url;
    @Value("${user}")
    String username;
    @Value("${password}")
    String password;

    @Bean
    public DataSource dataSource(){
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }
}
```

##### 3、MyBatisConfig

先获取SQLSessionFactory对象

设置数据源

设置日志文件

在resources包中添加log4j的配置文件

```java
//获取SQLSessionFactory对象
@Bean
public SqlSessionFactoryBean sessionFactoryBean(DataSource dataSource){

    SqlSessionFactoryBean ssfb = new SqlSessionFactoryBean();
    //设置数据源
    ssfb.setDataSource(dataSource);
    //设置日志文件
    Configuration configuration = new Configuration();
    configuration.setLogImpl(Log4jImpl.class);
    ssfb.setConfiguration(configuration);
    return ssfb;
}
```

然后扫描配置文件

```java
//扫描配置文件
@Bean
public MapperScannerConfigurer mapperScannerConfigurer(){
    MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
    mapperScannerConfigurer.setBasePackage("com.wang.mapper");
    return mapperScannerConfigurer;
}
```

添加注解@Bean，将它交给Spring管理

##### 4、SpringMvcConfig

给SpringMvcConfig添加注解：@Configuration//配置类

然后添加扫描主键的注解，扫描com.wang.controller层下所有的主键

```java
@Configuration
@ComponentScan("com.wang.controller")
public class SpringMvcConfig {
}
```

##### 5、WebServletConfig

继承 AbstractAnnotationConfigDispatcherServletInitializer实现里面三种方法

```java
public class WebServletConfig extends AbstractAnnotationConfigDispatcherServletInitializer {

    //加载spring配置
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{SpringConfig.class};
    }

    //加载springMVC配置
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{SpringMvcConfig.class};
    }

    //加载拦截规则
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
```

#### 2.controller层

#### 3.mapper

#### 4.entity

#### 5.aop

### 4.resources包

#### 1、在包中创建com.wang.mapper

写xml文件