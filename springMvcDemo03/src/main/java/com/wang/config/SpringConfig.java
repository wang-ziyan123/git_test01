package com.wang.config;

import org.springframework.context.annotation.*;
import org.springframework.stereotype.Controller;

/**
 * @author WangZiYan
 * @create 2023/10/9 15:05
 */
@Configuration
@ComponentScan(value = "com.wang",
        excludeFilters = @ComponentScan.Filter
                (type = FilterType.ANNOTATION,
                        classes = Controller.class))//扫描主键,过去controller层)
@PropertySource("classpath:jdbc.properties")
@Import({JDBCConfig.class,MyBatisConfig.class})
public class SpringConfig {
}
